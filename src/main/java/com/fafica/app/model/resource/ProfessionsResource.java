package com.fafica.app.model.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fafica.app.model.Person;
import com.fafica.app.model.Professions;
import com.fafica.app.model.repo.ProfessionsRepo;

@RestController
@RequestMapping("/professions")
public class ProfessionsResource {

	@Autowired
	private ProfessionsRepo pr;

	@GetMapping()
	public List<Professions> list() {
		return (List<Professions>) pr.findAll();
	}

	@GetMapping("/{id}")
	public Professions listProfession(@PathVariable("id") int id) {
		return pr.findById(id).get();
	}

	@PostMapping("/add")
	@ResponseStatus(HttpStatus.CREATED)
	public void newProfession(@RequestBody Professions p) {
		pr.save(p);
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable("id") int id) {
		pr.deleteById(id);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Professions> update(@PathVariable int id, @Valid @RequestBody Professions p) {
		Optional<Professions> save = pr.findById(id);
		BeanUtils.copyProperties(p, save.get(), "id");
		pr.save(save.get());
		return ResponseEntity.ok(save.get());
	}
	
}
