package com.fafica.app.model.repo;

import org.springframework.data.repository.CrudRepository;

import com.fafica.app.model.Professions;

public interface ProfessionsRepo extends CrudRepository<Professions, Integer>{

}
