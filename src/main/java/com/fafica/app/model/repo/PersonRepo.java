package com.fafica.app.model.repo;

import org.springframework.data.repository.CrudRepository;

import com.fafica.app.model.Person;

public interface PersonRepo extends CrudRepository<Person, Integer>{

}
