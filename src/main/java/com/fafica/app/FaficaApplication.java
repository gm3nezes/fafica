package com.fafica.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FaficaApplication {

	public static void main(String[] args) {
		SpringApplication.run(FaficaApplication.class, args);
	}
}
