package com.fafica.app.service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;

import com.fafica.app.model.Professions;
import com.fafica.app.model.repo.ProfessionsRepo;

public class ProfessionService {
	
	@Autowired
	ProfessionsRepo pr;
	
	public Professions update(int id, Professions p) {
		Professions save = searchProfessionID(id);
		BeanUtils.copyProperties(p, save, "id");
		return pr.save(save);
	}

	private Professions searchProfessionID(int id) {
		Optional<Professions> save = pr.findById(id);
		if(!save.isPresent()) {
			throw new EmptyResultDataAccessException(1);
		}
		return save.get();
	}

}
