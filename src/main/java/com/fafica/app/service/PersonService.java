package com.fafica.app.service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.fafica.app.model.Person;
import com.fafica.app.model.repo.PersonRepo;

@Service
public class PersonService {

	@Autowired
	PersonRepo pr;
	
	public Person update(int id, Person p) {
		Person save = searchPersonID(id);
		BeanUtils.copyProperties(p, save, "id");
		return pr.save(save);
	}

	private Person searchPersonID(int id) {
		Optional<Person> save = pr.findById(id);
		if(!save.isPresent()) {
			throw new EmptyResultDataAccessException(1);
		}
		return save.get();
	}
	
}
